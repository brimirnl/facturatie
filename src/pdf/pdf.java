/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdf;

import com.dikzeewolde.facturen.Factuur;
import com.dikzeewolde.klanten.Klant;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.xmlgraphics.util.MimeConstants;
/**
 *
 * @author jEROEN
 */
public class pdf {
    
    private static String pathToPdf = null;
    private static Boolean openAfter = true;
    
    public static String getFile(String file){
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(Toolkit.getDefaultToolkit().getClass().getResourceAsStream(file)));
            StringBuilder sb = new StringBuilder();
            String read;
            do{
                read = br.readLine();
                sb.append(read != null ? read+"\n" : "");
            } while(read != null);
            return sb.toString();
        } catch (Exception ex) {
        }
        return null;
    }

    public static String getXml(int id) {
        try {
            Factuur fact = new Factuur(id);
            Klant client = fact.getClient();
            ResultSet products = fact.getAllProducts();
            String xml = getFile("/invoice.xml");
            String productxml = getFile("/product.xml");
            String btwxmlfile = getFile("/btw.xml");
            String btwxml = "";
            String product = "";
            NumberFormat formatter = NumberFormat.getInstance(new Locale("nl", "NL"));
            formatter.setMaximumFractionDigits(2);
            formatter.setMinimumFractionDigits(2);
            Map<Float, Float> btw = new HashMap<>();

            while (products.next()) {
                if (!btw.containsKey(products.getFloat("tax") / 100)) {
                    btw.put(products.getFloat("tax") * 100, 0F);
                }
                btw.put(products.getFloat("tax") * 100,
                        btw.get(products.getFloat("tax") * 100) + ((products.getFloat("quantity") * products.getFloat("price_each") * products.getFloat("tax")))
                );
                product += String.format(productxml,
                        products.getString("quantity"),
                        products.getString("name"),
                        "€ " + formatter.format(Double.parseDouble(products.getString("price_each") + "")),
                        "€ " + formatter.format(Double.parseDouble((products.getFloat("quantity") * products.getFloat("price_each")) + "")),
                        products.isLast() && !fact.isBtw_moved() ? "<borderWidth>0.5mm</borderWidth>" : ""
                );
            }
            if (fact.isBtw_moved()) {
                product += String.format(productxml,
                        "",
                        "Btw is verlegd naar btw nr: " + client.getBtw_nr(),
                        "",
                        "",
                        "<borderWidth>0.5mm</borderWidth>"
                );
            }

            for (Map.Entry<Float, Float> entry : btw.entrySet()) {
                btwxml += String.format(btwxmlfile,
                        entry.getKey() + " %",
                        "€ " + formatter.format(Double.parseDouble(entry.getValue() + ""))
                );
            }

            xml = String.format(xml,
                    fact.getDescription() == null ? "" : fact.getDescription(),
                    id,
                    new SimpleDateFormat("dd-MM-yyyy").format(fact.getDate_added()),
                    client.getFull_name(),
                    client.getStreet(),
                    client.getZip() + " " + client.getTown(),
                    client.getCountry(),
                    "€ " + formatter.format(Double.parseDouble(fact.getTotalexcl() + "")),
                    "€ " + formatter.format(Double.parseDouble(fact.getTotalincl() + "")),
                    fact.isBtw_moved() ? 1 : 0,
                    fact.getCredit(),
                    btwxml,
                    product
            );
            return xml;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public static void createInvoicePdf(String xml, String name, String path, Boolean openAfter) {
        pdf.pathToPdf = path;
        pdf.openAfter = openAfter;
        createInvoicePdf(xml,name);
        openAfter = true;
        pathToPdf = null;
    }

    public static void createInvoicePdf(String xml, String name) {
        OutputStream out = null;
        try {
            InputStream xslFile = Toolkit.getDefaultToolkit().getClass().getResourceAsStream("/invoice.xsl");
            
            Preferences pref = Preferences.userRoot();
            if(pathToPdf == null){
                pathToPdf = pref.get("path", null);
            }
            
            File pdffile = new File(pathToPdf,name+".pdf");

            FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());

            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();

            out = new java.io.FileOutputStream(pdffile);
            out = new java.io.BufferedOutputStream(out);

            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xslFile));

            Result res = new SAXResult(fop.getDefaultHandler());

            transformer.transform(new StreamSource(new StringReader(xml)), res);
            if(openAfter){
                Desktop.getDesktop().open(new File(pathToPdf,name+".pdf"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
