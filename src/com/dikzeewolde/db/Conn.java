/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.db;

import com.dikzeewolde.facturatie.Alert;
import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author jEROEN
 */
public class Conn {
    private static Connection conn;
    public static Connection getConn(){
        if(conn == null){
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection("jdbc:mysql://149.210.244.72:3306/admin_default?user=admin_default&password=default_password");
            } catch (Exception ex) {
                Alert.Error("Er kan geen verbinding worden gemaakt met het database");
                System.exit(1);
            }
        }
        return conn;
    }
}

