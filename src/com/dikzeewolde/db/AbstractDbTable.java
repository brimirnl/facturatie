/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author jEROEN
 */
abstract public class AbstractDbTable {
    
    protected Connection conn;
    protected ResultSet rs;
    
    public AbstractDbTable(){
        conn = Conn.getConn();
    }
    abstract public boolean next();
    
    abstract public String[] getColumnHeadersForTable();
    
    abstract public List<List<String>> getColumns();
    
    abstract public AbstractDbRow get(int row);
}
