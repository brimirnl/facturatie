/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.facturatie;

import com.dikzeewolde.db.AbstractDbTable;
import com.dikzeewolde.db.AbstractDbRow;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author jEROEN
 */
public class MyTableModel extends AbstractTableModel {

    private List<List<String>> test;
    private String[] cNames;
    private AbstractDbTable table;
    
    public MyTableModel(AbstractDbTable table){
        this.table = table;
        cNames = table.getColumnHeadersForTable();
        test = table.getColumns();
    }
    
    @Override
    public int getRowCount() {
        return test.size();
    }

    @Override
    public int getColumnCount() {
        return cNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return test.get(rowIndex).get(columnIndex);
    }

    @Override
    public String getColumnName(int ind) {
        return cNames[ind];
    }
    
    public AbstractDbRow getObjectAtRow(int row){
        return table.get(row);
    }
    
    public void addRow(){
        List<String> x = new ArrayList<>();
        for(int index = 0; index < test.get(0).size(); index++){
            x.add("");
        }
        test.add(x);
    }
}
