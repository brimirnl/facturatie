/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.facturatie;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author jEROEN
 */
public class TestModel extends AbstractTableModel {

    private List<List<String>> rowdata = new ArrayList<>();
    private List<String> columns = new ArrayList<>();

    public TestModel() {
        columns.add("Aantal");
        columns.add("Omschrijving");
        columns.add("BTW");
        columns.add("Prijs per stuk");

        List<String> row = new ArrayList<>();
        row.add("");
        row.add("");
        row.add("");
        row.add("");
        rowdata.add(row);
    }

    public void add() {
        List<String> row = new ArrayList<>();
        row.add("");
        row.add("");
        row.add("");
        row.add("");
        rowdata.add(row);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return true;
    }

    @Override
    public int getRowCount() {
        return rowdata.size();
    }

    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return rowdata.get(rowIndex).get(columnIndex);
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        rowdata.get(rowIndex).set(columnIndex, value.toString());
    }

    @Override
    public String getColumnName(int index) {
        return columns.get(index);
    }
    
    public void removeRows(int[] rows){
        int i = 0;
        for(int row : rows){
            rowdata.remove(row - i++);
        }
    }
    
    public List<List<String>> getAllValues(){
        return rowdata;
    }
}
