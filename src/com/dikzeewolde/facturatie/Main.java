/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.facturatie;

import java.io.File;
import java.util.prefs.Preferences;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 *
 * @author jEROEN
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    private final Preferences pref = Preferences.userRoot();

    public static void main(String[] args) {
        new Main();
    }

    public Main() {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        JFileChooser chooser = new JFileChooser();
        String path = pref.get("path", null);
        if (path == null) {
            chooser.setCurrentDirectory(new File("."));
            chooser.setAcceptAllFileFilterUsed(false);
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            JFrame frame = new JFrame();
            if (chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
                pref.put("path", chooser.getSelectedFile().getAbsolutePath());
                openApplication();
            } else {
                JOptionPane.showMessageDialog(null, "Er moet een map gekozen worden",
                        "Geen map gekozen", JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            }
            frame.dispose();
        } else {
            openApplication();
        }
    }

    public void openApplication() {
        new Frame();
    }
}
