/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.facturatie;

import javax.swing.JOptionPane;

/**
 *
 * @author jEROEN
 */
public class Alert {
    public static void Error(String msg){
        JOptionPane.showConfirmDialog(null,
                        msg, 
                        "-",
                        JOptionPane.PLAIN_MESSAGE,
                        JOptionPane.ERROR_MESSAGE);
    }
    
    public static void Error(String msg, String title){
        JOptionPane.showConfirmDialog(null,
                        msg, 
                        title,
                        JOptionPane.PLAIN_MESSAGE,
                        JOptionPane.ERROR_MESSAGE);
    }
    public static void Notify(String msg){
        JOptionPane.showConfirmDialog(null,
                        msg, 
                        "-",
                        JOptionPane.PLAIN_MESSAGE,
                        JOptionPane.PLAIN_MESSAGE);
    }
}
