/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.klanten;

import com.dikzeewolde.db.AbstractDbRow;
import com.dikzeewolde.facturatie.Alert;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Jeroen van Barneveld
 */
public class Klant extends AbstractDbRow {

    protected int id;
    protected String comp_name;
    protected String first_name;
    protected String prefix;
    protected String last_name;
    protected String zip;
    protected String street;
    protected String house_nr;
    protected String town;
    protected String phone_number;
    protected String email;
    protected String btw_nr;
    protected String country;

    public Klant(ResultSet rs) {
        super();
        init(rs);
    }

    public Klant(int id) {
        super();
        try {
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM clients WHERE id=?");
            prep.setInt(1, id);
            ResultSet rs = prep.executeQuery();
            rs.first();
            init(rs);
        } catch (SQLException ex) {
            Alert.Error("Klant kon niet worden opgehaald");
        }
    }

    public Klant() {
        super();
        this.id = 0;
    }

    private void init(ResultSet rs) {
        try {
            id = rs.getInt("id");
            comp_name = rs.getString("comp_name");
            first_name = rs.getString("first_name");
            prefix = rs.getString("prefix");
            last_name = rs.getString("last_name");
            zip = rs.getString("zip");
            street = rs.getString("street");
            house_nr = rs.getString("house_nr");
            town = rs.getString("town");
            phone_number = rs.getString("phone_number");
            email = rs.getString("email");
            btw_nr = rs.getString("btw_nr");
            country = rs.getString("country");
        } catch (SQLException ex) {
            Alert.Error("Error setting data for client");
            ex.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getFull_name() {
        if(getComp_name() == null  || getComp_name().equals(" ") || getComp_name().equals("")){
            return first_name + (prefix == null ? " " : " " + prefix + " ") + last_name;
        } else{
            return getComp_name();
        }
    }

    public String getComp_name() {
        return comp_name;
    }

    public void setComp_name(String comp_name) {
        this.comp_name = comp_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse_nr() {
        return house_nr;
    }

    public void setHouse_nr(String house_nr) {
        this.house_nr = house_nr;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBtw_nr() {
        return btw_nr;
    }

    public void setBtw_nr(String btw_nr) {
        this.btw_nr = btw_nr;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    public String getFull_address(){
        return String.format("%s %s\n%s %s\n%s",street,house_nr,zip,town,country);
    }

    public void save() {
        try {
            PreparedStatement prep;
            String sql;
            if (id == 0) {
                sql = "INSERT INTO clients(comp_name, first_name, prefix, last_name,"
                        + "zip, street, house_nr, town, phone_number, email, btw_nr,"
                        + "country) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                prep = conn.prepareStatement(sql);
            } else {
                sql = "UPDATE clients SET comp_name=?,first_name=?,prefix=?,last_name=?,"
                        + "zip=?,street=?,house_nr=?,town=?,phone_number=?,email=?,"
                        + "btw_nr=?,country=? WHERE id=?";
                prep = conn.prepareStatement(sql);
                prep.setInt(13, id);
            }
            prep.setString(1, comp_name);
            prep.setString(2, first_name);
            prep.setString(3, prefix);
            prep.setString(4, last_name);
            prep.setString(5, zip);
            prep.setString(6, street);
            prep.setString(7, house_nr);
            prep.setString(8, town);
            prep.setString(9, phone_number);
            prep.setString(10, email);
            prep.setString(11, btw_nr);
            prep.setString(12, country);
            prep.executeUpdate();
        } catch (Exception ex) {
            Alert.Error("Error while saving new client");
            ex.printStackTrace();
        }
    }
}
