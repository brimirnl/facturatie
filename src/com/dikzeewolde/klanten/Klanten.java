/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.klanten;

import com.dikzeewolde.db.AbstractDbTable;
import com.dikzeewolde.facturatie.Alert;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jEROEN
 */
public class Klanten extends AbstractDbTable{
    
    public Klanten(){
        super();
        try {
            Statement stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM clients");
            rs.beforeFirst();
        } catch (SQLException ex) {
            Alert.Error("Er is iets fout gegaan bij het ophalen van de klanten");
        }
    }
    @Override
    public boolean next() {
        try {
            return rs.next();
        } catch (SQLException ex) {
            Alert.Error("Er is iets fout gegaan met de SQL");
        }
        return false;
    }
    public Klant get(){
        return new Klant(rs);
    }
    
    
    @Override
    public Klant get(int row){
        try {
            rs.absolute(row);
            if(rs.next()){
                return new Klant(rs);
            } else{
                return new Klant();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Klanten.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("test");
        }
        return null;
    }
    @Override
    public String[] getColumnHeadersForTable() {
        String[] result = {"Bedrijfsnaam","Naam","Btw nr"};
        return result;
    }
    
    @Override
    public List<List<String>> getColumns(){
        try {
            List<List<String>> columns = new ArrayList<>();
            rs.beforeFirst();
            while(rs.next()){
                Klant klant = new Klant(rs);
                List<String> column = new ArrayList<>();
                column.add(klant.getComp_name());
                column.add(klant.getFull_name());
                column.add(klant.getBtw_nr());
                columns.add(column);
            }
            return columns;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
