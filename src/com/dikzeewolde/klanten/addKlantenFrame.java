/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.klanten;

import com.dikzeewolde.facturatie.Frame;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Jeroen van Barneveld
 */
public class addKlantenFrame {

    private JPanel headPanel;
    private JFrame frame;

    private JTextField textComp_name;
    private JLabel labelComp_name;
    private JTextField textFirst_name;
    private JLabel labelFirst_name;
    private JTextField textPrefix;
    private JLabel labelPrefix;
    private JTextField textLast_name;
    private JLabel labelLast_name;
    private JTextField textZip;
    private JLabel labelZip;
    private JTextField textStreet;
    private JLabel labelStreet;
    private JTextField textHouse_nr;
    private JLabel labelHouse_nr;
    private JTextField textTown;
    private JLabel labelTown;
    private JTextField textPhone_number;
    private JLabel labelPhone_number;
    private JTextField textEmail;
    private JLabel labelEmail;
    private JTextField textBtw_nr;
    private JLabel labelBtw_nr;
    private JTextField textCountry;
    private JLabel labelCountry;

    private int clientId = 0;

    public addKlantenFrame() {
        init();
    }
    
    public addKlantenFrame(int clientId) {
        this.clientId = clientId;
        init();
    }
    public void init() {
        frame = new JFrame("test");

        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JPanel watisdeze = new JPanel();
        headPanel = new JPanel(new GridLayout(13, 1, 0, 0));
        watisdeze.add(headPanel);

        AddButtons();

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JButton button = new JButton("Opslaan");
        button.addActionListener((ActionEvent e) -> {
            frame.dispose();
            Klant klant;
            if(clientId != 0){
                klant = new Klant(clientId);
            } else{
                klant = new Klant();
            }
            klant.setComp_name(textComp_name.getText());
            klant.setFirst_name(textFirst_name.getText());
            klant.setPrefix(textPrefix.getText());
            klant.setLast_name(textLast_name.getText());
            klant.setZip(textZip.getText());
            klant.setStreet(textStreet.getText());
            klant.setHouse_nr(textHouse_nr.getText());
            klant.setTown(textTown.getText());
            klant.setPhone_number(textPhone_number.getText());
            klant.setEmail(textEmail.getText());
            klant.setBtw_nr(textBtw_nr.getText());
            klant.setCountry(textCountry.getText());
            klant.save();
            Frame.openKlantenFrame();
        });
        panel.add(button);
        headPanel.add(panel);
        frame.add(watisdeze);
        frame.pack();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width / 2 - frame.getSize().width / 2,
                dim.height / 2 - frame.getSize().height / 2);
    }

    public void AddButtons() {
        //String[] test = {"id","comp_name","first_name","prefix","last_name","zip","street","house_nr","town","phone_number","email","btw_nr"};
        Klant klant = new Klant();
        if(clientId != 0){
            klant = new Klant(clientId);
        }
        JPanel panel;

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelComp_name = new JLabel("Bedrijfsnaam");
        textComp_name = new JTextField();
        if (clientId != 0) {
            textComp_name.setText(klant.getComp_name());
        }
        textComp_name.setPreferredSize(new Dimension(200, 20));
        panel.add(labelComp_name);
        panel.add(textComp_name);
        headPanel.add(panel);

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelFirst_name = new JLabel("Voornaam");
        textFirst_name = new JTextField();
        if (clientId != 0) {
            textFirst_name.setText(klant.getFirst_name());
        }
        textFirst_name.setPreferredSize(new Dimension(200, 20));
        panel.add(labelFirst_name);
        panel.add(textFirst_name);
        headPanel.add(panel);

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelPrefix = new JLabel("Tussenvoegsel");
        textPrefix = new JTextField();
        if (clientId != 0) {
            textPrefix.setText(klant.getPrefix());
        }
        textPrefix.setPreferredSize(new Dimension(200, 20));
        panel.add(labelPrefix);
        panel.add(textPrefix);
        headPanel.add(panel);

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelLast_name = new JLabel("Achternaam");
        textLast_name = new JTextField();
        if (clientId != 0) {
            textLast_name.setText(klant.getLast_name());
        }
        textLast_name.setPreferredSize(new Dimension(200, 20));
        panel.add(labelLast_name);
        panel.add(textLast_name);
        headPanel.add(panel);

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelZip = new JLabel("Postcode");
        textZip = new JTextField();
        if (clientId != 0) {
            textZip.setText(klant.getZip());
        }
        textZip.setPreferredSize(new Dimension(200, 20));
        panel.add(labelZip);
        panel.add(textZip);
        headPanel.add(panel);

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelStreet = new JLabel("Straat");
        textStreet = new JTextField();
        if (clientId != 0) {
            textStreet.setText(klant.getStreet());
        }
        textStreet.setPreferredSize(new Dimension(200, 20));
        panel.add(labelStreet);
        panel.add(textStreet);
        headPanel.add(panel);

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelHouse_nr = new JLabel("Huisnummer");
        textHouse_nr = new JTextField();
        if (clientId != 0) {
            textHouse_nr.setText(klant.getHouse_nr());
        }
        textHouse_nr.setPreferredSize(new Dimension(200, 20));
        panel.add(labelHouse_nr);
        panel.add(textHouse_nr);
        headPanel.add(panel);

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelTown = new JLabel("Plaats");
        textTown = new JTextField();
        if (clientId != 0) {
            textTown.setText(klant.getTown());
        }
        textTown.setPreferredSize(new Dimension(200, 20));
        panel.add(labelTown);
        panel.add(textTown);
        headPanel.add(panel);

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelPhone_number = new JLabel("Tel nr.");
        textPhone_number = new JTextField();
        if (clientId != 0) {
            textPhone_number.setText(klant.getPhone_number());
        }
        textPhone_number.setPreferredSize(new Dimension(200, 20));
        panel.add(labelPhone_number);
        panel.add(textPhone_number);
        headPanel.add(panel);

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelEmail = new JLabel("Email");
        textEmail = new JTextField();
        if (clientId != 0) {
            textEmail.setText(klant.getEmail());
        }
        textEmail.setPreferredSize(new Dimension(200, 20));
        panel.add(labelEmail);
        panel.add(textEmail);
        headPanel.add(panel);

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelBtw_nr = new JLabel("Btw nr");
        textBtw_nr = new JTextField();
        if (clientId != 0) {
            textBtw_nr.setText(klant.getBtw_nr());
        }
        textBtw_nr.setPreferredSize(new Dimension(200, 20));
        panel.add(labelBtw_nr);
        panel.add(textBtw_nr);
        headPanel.add(panel);

        panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelCountry = new JLabel("Land");
        textCountry = new JTextField();
        textCountry.setText("Nederland");
        if (clientId != 0) {
            textCountry.setText(klant.getCountry());
        }
        textCountry.setPreferredSize(new Dimension(200, 20));
        panel.add(labelCountry);
        panel.add(textCountry);
        headPanel.add(panel);
    }

    public void toFront() {
        if (frame.isVisible()) {
            frame.toFront();
        } else {
            init();
        }
    }
}
