/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.facturen;

import com.dikzeewolde.db.Conn;
import com.dikzeewolde.facturatie.Alert;
import com.dikzeewolde.facturatie.Frame;
import com.dikzeewolde.facturatie.MyTableModel;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.prefs.Preferences;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.table.TableRowSorter;
import pdf.pdf;

/**
 *
 * @author jEROEN
 */
public class RightClickMenu extends MouseAdapter {

    private JTable table;
    private MyTableModel model;
    private TableRowSorter sorter;
    private Facturen facturen;
    private int row;

    public RightClickMenu(JTable table, MyTableModel model, TableRowSorter sorter, Facturen facturen) {
        this.table = table;
        this.model = model;
        this.sorter = sorter;
        this.facturen = facturen;
    }

    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON3) {
            row = table.rowAtPoint(e.getPoint());
            table.changeSelection(row, row, false, false);
            row = sorter.convertRowIndexToModel(row);
            Factuur fact = (Factuur) facturen.get(row);

            JPopupMenu popup = new JPopupMenu();
            JMenuItem menuItem = new JMenuItem("Maak een krediet factuur");
            menuItem.addActionListener(new RightClickMenuAction());
            popup.add(menuItem);
            menuItem = new JMenuItem("Open PDF");
            menuItem.addActionListener(new RightClickMenuAction());
            popup.add(menuItem);
            if(!fact.isFull_paid()){
                menuItem = new JMenuItem("Zet op betaald");
                menuItem.addActionListener(new RightClickMenuAction());
                popup.add(menuItem);
            }
            popup.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    class RightClickMenuAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand() == "Maak een krediet factuur") {
                createCreditInvoice(e);
            } else if (e.getActionCommand() == "Open PDF"){
                OpenPdf(e);
            } else if(e.getActionCommand() == "Zet op betaald"){
                setPaid(e);
            }
        }
        
        private void setPaid(ActionEvent e){
            Factuur fact = (Factuur) facturen.get(row);
            fact.setFull_paid(true);
            fact.save();
            Frame.openInvoiceNotPaidFrame();
        }

        private void OpenPdf(ActionEvent e){
            Factuur fact = (Factuur) facturen.get(row);
            Preferences pref = Preferences.userRoot();
            String path = pref.get("path",null);
            File file = new File(path,"Factuur "+fact.getId()+".pdf");
            if(file.exists()){
                try {
                    Desktop.getDesktop().open(file);
                } catch (Exception ex) {
                }
            } else{
                String xml = pdf.getXml(fact.getId());
                pdf.createInvoicePdf(xml, "Factuur "+fact.getId());
            }
        }
        
        private void createCreditInvoice(ActionEvent e) {
            try {
                Factuur oldFact = (Factuur) facturen.get(row);
                
                PreparedStatement prep = Conn.getConn().prepareStatement("SELECT COUNT(*) AS aantal FROM invoices WHERE credit=?");
                prep.setInt(1,oldFact.getId());
                ResultSet rs = prep.executeQuery();
                rs.first();
                
                if (oldFact.getCredit() != 0) {
                    Alert.Notify("Dit is al een krediet factuur");
                } else if(rs.getInt("Aantal") > 0){
                    Alert.Notify("Van deze factuur is al een krediet factuur gemaakt");
                } else {
                    Factuur newFact = new Factuur();
                    Connection conn = Conn.getConn();
                    prep = conn.prepareStatement("INSERT INTO `invoice_product`(`invoice_id`, `quantity`, `name`, `price_each`, `tax`) VALUES (?,?,?,?,?)");

                    newFact.setTotalexcl(oldFact.getTotalexcl() - (oldFact.getTotalexcl() * 2));
                    newFact.setTotalincl(oldFact.getTotalincl() - (oldFact.getTotalincl() * 2));
                    newFact.setBtw_moved(oldFact.isBtw_moved());
                    newFact.setClient_id(oldFact.getClient_id());
                    newFact.setCredit(oldFact.getId());
                    int newId = newFact.save();
                    prep.setInt(1, newId);
                    prep.setFloat(2, 1);
                    prep.setString(3, "Krediet factuur van factuur: " + oldFact.getId());
                    float price = oldFact.isBtw_moved() ? oldFact.getTotalexcl() - (oldFact.getTotalexcl() * 2) : oldFact.getTotalincl() - (oldFact.getTotalincl() * 2);
                    prep.setFloat(4, price);
                    prep.setFloat(5, 0);
                    prep.executeUpdate();
                    String xml = pdf.getXml(newId);
                    pdf.createInvoicePdf(xml, "Factuur " + newId);
                    Frame.openInvoicePaidFrame();
                    Alert.Notify("Factuur is toegevoegd");
                }
            } catch (SQLException ex) {
            }
        }
    }
}
