/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.facturen;

import com.dikzeewolde.db.AbstractDbRow;
import com.dikzeewolde.facturatie.Alert;
import com.dikzeewolde.klanten.Klant;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author jEROEN
 */
public class Factuur extends AbstractDbRow {

    private int id;
    private int client_id;
    private Date date_added;
    private double paid;
    private String description;
    private boolean btw_moved;
    private boolean full_paid;
    private float totalexcl;
    private float totalincl;
    private int credit;

    public Factuur(ResultSet rs) {
        super();
        init(rs);
    }

    public Factuur(int id) {
        super();
        try {
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM invoices WHERE id=?");
            prep.setInt(1, id);
            ResultSet rs = prep.executeQuery();
            rs.first();
            init(rs);
        } catch (SQLException ex) {
            Alert.Error("Factuur kon niet worden opgehaald");
        }
    }

    public Factuur() {
        super();
        this.id = 0;
    }

    private void init(ResultSet rs) {
        try {
            id = rs.getInt("id");
            client_id = rs.getInt("client_id");
            date_added = rs.getDate("date_added");
            paid = rs.getDouble("paid");
            description = rs.getString("description");
            btw_moved = rs.getBoolean("btw_moved");
            full_paid = rs.getBoolean("full_paid");
            totalexcl = rs.getFloat("totalexcl");
            totalincl = rs.getFloat("totalincl");
            credit = rs.getInt("credit");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public int getId(){
        return id;
    }
    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public Date getDate_added() {
        return date_added;
    }

    public void setDate_added(Date date_added) {
        this.date_added = date_added;
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isBtw_moved() {
        return btw_moved;
    }

    public void setBtw_moved(boolean btw_moved) {
        this.btw_moved = btw_moved;
    }

    public boolean isFull_paid() {
        return full_paid;
    }

    public void setFull_paid(boolean full_paid) {
        this.full_paid = full_paid;
    }

    public float getTotalexcl() {
        return totalexcl;
    }

    public void setTotalexcl(float totalexcl) {
        this.totalexcl = totalexcl;
    }

    public float getTotalincl() {
        return totalincl;
    }

    public void setTotalincl(float totalincl) {
        this.totalincl = totalincl;
    }
    
    public ResultSet getAllProducts(){
        try {
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM invoice_product WHERE invoice_id=?");
            prep.setInt(1,id);
            return prep.executeQuery();
        } catch (SQLException ex) {
        }
        return  null;
    }
    
    public Klant getClient(){
        return new Klant(getClient_id());
    }

    public int save() {
        try {
            PreparedStatement prep;
            if (this.id == 0) {
                prep = conn.prepareStatement("INSERT INTO `invoices`(`client_id`, `date_added`, `paid`, `description`, `btw_moved`, `full_paid`, `credit`, `totalexcl`, `totalincl`)"
                        + "VALUES (?,now(),?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
            } else {
                prep = conn.prepareStatement("UPDATE `invoices` SET `client_id`=?,`date_added`=NOW(),`paid`=?,`description`=?,`btw_moved`=?,`full_paid`=?, `credit`=?, `totalexcl`=?,`totalincl`=? WHERE id=?",Statement.RETURN_GENERATED_KEYS);
                prep.setInt(9, id);
            }
            prep.setInt(1, client_id);
            prep.setDouble(2, paid);
            prep.setString(3, description);
            prep.setBoolean(4, btw_moved);
            prep.setBoolean(5, full_paid);
            prep.setInt(6, credit);
            prep.setFloat(7, totalexcl);
            prep.setFloat(8, totalincl);
            prep.executeUpdate();
            ResultSet id = prep.getGeneratedKeys();
            id.next();
            return this.id==0 ? id.getInt(1) : this.id;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }
}
