/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.facturen;

import com.dikzeewolde.db.Conn;
import com.dikzeewolde.facturatie.Alert;
import com.dikzeewolde.facturatie.TestModel;
import com.dikzeewolde.klanten.Klant;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import pdf.pdf;

/**
 *
 * @author jEROEN
 */
public class addFactuurFrame {

    private JFrame frame;
    private static Klant selectedKlant;
    private selectClientFrame selectClientFrame;
    private static JTextField clientName;
    private static JTextArea addressArea;
    private TestModel model;
    private JCheckBox taxMoved;
    private JTextArea opmerkingen;

    public addFactuurFrame() {
        init();
    }

    public static void setKlantAndUpdate(Klant klant) {
        selectedKlant = klant;
        System.out.println(String.format("'%s'",klant.getComp_name()));
        clientName.setText(klant.getComp_name() == null || klant.getComp_name().equals(" ") || klant.getComp_name().equals("") ? klant.getFull_name() : klant.getComp_name());
        addressArea.setText(klant.getFull_address());
    }

    private void init() {
        frame = new JFrame("");
        JPanel hoofdFrame = new JPanel(new GridBagLayout());
        GridBagConstraints hoofdFrameGbc = new GridBagConstraints();

        JPanel panel;
        GridBagConstraints gbc;
        hoofdFrameGbc.gridx = 0;

        /*
         Start Panel 1;
         */
        panel = new JPanel(new GridBagLayout());
        gbc = new GridBagConstraints();
        gbc.anchor = gbc.NORTHWEST;

        hoofdFrameGbc.gridy = 0;
        JLabel clientLabelName = new JLabel("Klantnaam:");
        gbc.insets = new Insets(0, 10, 10, 0);
        gbc.gridy = 0;
        gbc.gridx = 0;
        panel.add(clientLabelName, gbc);

        clientName = new JTextField();
        clientName.setPreferredSize(new Dimension(160, 20));
        clientName.setEditable(false);
        gbc.insets = new Insets(0, 10, 10, 0);
        gbc.gridy = 0;
        gbc.gridx = 1;
        panel.add(clientName, gbc);

        JLabel addressLabel = new JLabel("Adres gegevens");
        gbc.insets = new Insets(0, 10, 10, 0);
        gbc.gridy = 1;
        gbc.gridx = 0;
        panel.add(addressLabel, gbc);

        try {
            URL clientsUrl = Toolkit.getDefaultToolkit().getClass().getResource("/clients.png");
            JLabel personIcon = new JLabel(new ImageIcon(clientsUrl));
            personIcon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            gbc.gridy = 0;
            gbc.gridx = 2;
            personIcon.setPreferredSize(new Dimension(32, 32));
            personIcon.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    if (selectClientFrame == null) {
                        selectClientFrame = new selectClientFrame();
                    } else {
                        selectClientFrame.toFront();
                    }
                }
            });
            panel.add(personIcon, gbc);
        } catch (Exception ex) {
        }

        addressArea = new JTextArea();
        addressArea.setPreferredSize(new Dimension(200, 200));
        addressArea.setEditable(false);
        addressArea.setBackground(new Color(238,238,238));
        addressArea.setBorder(BorderFactory.createLineBorder(new Color(200, 200, 200)));
        gbc.insets = new Insets(0, 10, 0, 0);
        gbc.gridy = 1;
        gbc.gridx = 1;
        gbc.gridwidth = 2;
        panel.add(addressArea, gbc);

        taxMoved = new JCheckBox("Btw verlegd");
        gbc.insets = new Insets(0, 0, 0, 0);
        gbc.gridy = 2;
        gbc.gridx = 1;
        panel.add(taxMoved, gbc);

        JLabel note = new JLabel("Evt. opmerkingen");
        gbc.insets = new Insets(0, 10, 10, 0);
        gbc.gridy = 0;
        gbc.gridx = 3;
        panel.add(note, gbc);

        opmerkingen = new JTextArea();
        opmerkingen.setPreferredSize(new Dimension(200, 220));
        opmerkingen.setBorder(BorderFactory.createLineBorder(new Color(200, 200, 200)));
        gbc.insets = new Insets(0, 10, 0, 0);
        gbc.gridy = 1;
        gbc.gridx = 3;
        gbc.gridheight = 2;
        panel.add(opmerkingen, gbc);

        hoofdFrame.add(panel, hoofdFrameGbc);
        /*
         end Panel 1;
         start Panel 2;
         */
        panel = new JPanel();

        model = new TestModel();
        JTable table = new JTable(model);

        JScrollPane pane = new JScrollPane(table);
        pane.setViewportView(table);
        table.getTableHeader().setReorderingAllowed(false);
        pane.setVerticalScrollBarPolicy(pane.VERTICAL_SCROLLBAR_ALWAYS);
        pane.setPreferredSize(new Dimension(600, 200));
        panel.add(pane);

        hoofdFrameGbc.gridy = 1;
        hoofdFrame.add(panel, hoofdFrameGbc);

        /*
         end Panel 2;
         start Panel 3;
         */
        panel = new JPanel(new GridBagLayout());
        hoofdFrameGbc.gridy = 3;
        gbc = new GridBagConstraints();

        JButton b = new JButton("product toevoegen");
        b.addActionListener((ActionEvent e) -> {
            model.add();
            table.updateUI();
        });
        panel.add(b, gbc);

        b = new JButton("Verwijder geselecteerde rijen");
        b.addActionListener((ActionEvent e) -> {
            int[] rows = table.getSelectedRows();
            model.removeRows(rows);
            table.updateUI();
        });
        gbc.insets = new Insets(0, 10, 0, 0);
        gbc.gridy = 0;
        gbc.gridx = 1;
        panel.add(b, gbc);
        hoofdFrameGbc.anchor = gbc.NORTHEAST;
        hoofdFrame.add(panel, hoofdFrameGbc);

        b = new JButton("Factuur maken");
        b.addActionListener((ActionEvent e) -> {
            if (table.isEditing()) {
                table.getCellEditor().stopCellEditing();
            }
            saveInvoice();
        });
        gbc.insets = new Insets(0, 10, 0, 0);
        gbc.gridy = 0;
        gbc.gridx = 2;
        panel.add(b, gbc);
        hoofdFrameGbc.anchor = gbc.NORTHEAST;
        hoofdFrame.add(panel, hoofdFrameGbc);

        /*
         end Panel 3;
         */
        frame.add(hoofdFrame);
        frame.pack();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width / 2 - frame.getSize().width / 2,
                dim.height / 2 - frame.getSize().height / 2);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);
    }

    public void toFront() {
        if (frame.isVisible()) {
            frame.toFront();
        } else {
            init();
        }
    }

    private void saveInvoice() {
        if (selectedKlant == null) {
            Alert.Error("Er moet nog een klant geselecteerd worden", "Geen klant");
            return;
        }
        if (model.getRowCount() == 0) {
            Alert.Error("Er moet minimaal 1 product toegevoegd worden");
            return;
        }
        if (model.getRowCount() == 1) {
            for (List data : model.getAllValues()) {
                for (Object item : data) {
                    if (item.equals("")) {
                        /*Alert.Error("Er moet minimaal 1 product worden toegevoegd. Alle waardes moeten ingevuld zijn.");
                         return;*/
                    }
                }
            }
        }
        float totalexcl = 0;
        float totalincl = 0;
        List<List<String>> rowdata = model.getAllValues();
        for (List item : rowdata) {
            try {
                float quantity = Float.parseFloat((String) item.get(0));
                float tax = (Float.parseFloat((String) item.get(2)) / 100) + 1;
                float price = Float.parseFloat((String) item.get(3));
                totalincl += quantity * (price * tax);
                totalexcl += quantity * price;
            }
            catch(Exception e){
                Alert.Error("Er staat een teken in een veld waar dit niet de bedoeling is");
                return;
            }
        }
        Factuur fact = new Factuur();
        fact.setTotalexcl(totalexcl);
        fact.setTotalincl(totalincl);
        fact.setBtw_moved(taxMoved.isSelected() ? true : false);
        fact.setDescription(opmerkingen.getText());
        fact.setClient_id(selectedKlant.getId());
        int id = fact.save();
        for (List item : rowdata) {
            try {
                float quantity = Float.parseFloat((String) item.get(0));
                Object name = item.get(1);
                float tax = (Float.parseFloat((String) item.get(2)) / 100);
                float price = Float.parseFloat((String) item.get(3));
                Connection conn = Conn.getConn();
                PreparedStatement prep = conn.prepareCall("INSERT INTO invoice_product (invoice_id, quantity, name, price_each, tax) VALUES (?,?,?,?,?)");
                prep.setInt(1, id);
                prep.setFloat(2, quantity);
                prep.setString(3, name.toString());
                prep.setFloat(4, price);
                prep.setFloat(5, tax);
                prep.executeUpdate();
            }
            catch(Exception e){
                Alert.Error("Er staat een teken in een veld waar dit niet de bedoeling is");
                e.printStackTrace();
                return;
            }
        }
        String xml = pdf.getXml(id);
        pdf.createInvoicePdf(xml, "Factuur "+id);
        Alert.Notify("Factuur is toegevoegd");
        frame.dispose();
    }
}

