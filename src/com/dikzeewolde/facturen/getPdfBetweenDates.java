/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.facturen;

import com.dikzeewolde.db.Conn;
import com.dikzeewolde.facturatie.Alert;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import pdf.pdf;

/**
 *
 * @author jEROEN
 */
public class getPdfBetweenDates {

    public getPdfBetweenDates() {
        UtilDateModel modelFrom = new UtilDateModel();
        UtilDateModel modelTill = new UtilDateModel();

        Properties p = new Properties();
        p.put("text.today", "Vandaag");
        p.put("text.month", "Maand");
        p.put("text.year", "Jaar");
        JDatePanelImpl datePanelFrom = new JDatePanelImpl(modelFrom, p);
        JDatePanelImpl datePanelTill = new JDatePanelImpl(modelTill, p);

        JDatePickerImpl datePickerFrom = new JDatePickerImpl(datePanelFrom, new getPdfBetweenDates.DateLabelFormatter());
        JDatePickerImpl datePickerTill = new JDatePickerImpl(datePanelTill, new getPdfBetweenDates.DateLabelFormatter());

        JTextField path = new JTextField();
        path.setPreferredSize(new Dimension(200, 20));

        JFrame frame = new JFrame("Kies datums");
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        JButton b = new JButton("Sla pdfs op");
        b.addActionListener((ActionEvent e) -> {
            try {
                if (datePickerFrom.getModel().getValue() == null || datePickerTill.getModel().getValue() == null || path.getText() == null || path.getText().equals("") || path.getText().equals(" ")) {
                    Alert.Error("De datums en het pad moeten ingevuld zijn.");
                    return;
                }
                String yearFrom = datePickerFrom.getModel().getYear() + "";
                String monthFrom = (datePickerFrom.getModel().getMonth() + 1) + "";
                String dayFrom = datePickerFrom.getModel().getDay() + "";
                String dateFrom = String.format("%s-%s-%s",
                        yearFrom,
                        monthFrom.length() == 2 ? monthFrom : "0" + monthFrom,
                        dayFrom.length() == 2 ? monthFrom : "0" + dayFrom
                );
                String yearTill = datePickerTill.getModel().getYear() + "";
                String monthTill = (datePickerTill.getModel().getMonth() + 1) + "";
                String dayTill = datePickerTill.getModel().getDay() + "";
                String dateTill = String.format("%s-%s-%s",
                        yearTill,
                        monthTill.length() == 2 ? monthTill : "0" + monthTill,
                        dayTill.length() == 2 ? dayTill : "0" + dayTill
                );
                PreparedStatement prep = Conn.getConn().prepareCall("SELECT * FROM invoices WHERE date_added BETWEEN ? AND ?");
                prep.setString(1, dateFrom);
                prep.setString(2, dateTill);
                ResultSet rs = prep.executeQuery();
                frame.dispose();
                Alert.Notify("Een ogenblik de pdfs worden gegenereerd");
                while (rs.next()) {
                    String xml = pdf.getXml(rs.getInt("id"));
                    pdf.createInvoicePdf(xml, "Factuur " + rs.getInt("id"), path.getText(), false);
                }
                Alert.Notify("De PDFS zijn gegeneerd!");
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        });

        panel.add(new JLabel("Kies datum van: "), gbc);
        gbc.gridy = 0;
        gbc.gridx = 1;
        panel.add(datePickerFrom, gbc);
        gbc.gridy = 1;
        gbc.gridx = 0;
        panel.add(new JLabel("Kies datum tot: "), gbc);
        gbc.gridy = 1;
        gbc.gridx = 1;
        panel.add(datePickerTill, gbc);
        gbc.gridy = 2;
        gbc.gridx = 0;
        gbc.gridwidth = 2;
        gbc.anchor = gbc.NORTHWEST;
        panel.add(path, gbc);
        gbc.gridy = 3;
        gbc.gridx = 0;
        gbc.anchor = gbc.NORTHEAST;
        panel.add(b, gbc);

        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width / 2 - frame.getSize().width / 2,
                dim.height / 2 - frame.getSize().height / 2);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    static class DateLabelFormatter extends JFormattedTextField.AbstractFormatter {

        private String datePattern = "yyyy-MM-dd";
        private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

        @Override
        public Object stringToValue(String text) throws ParseException {
            return dateFormatter.parseObject(text);
        }

        @Override
        public String valueToString(Object value) throws ParseException {
            if (value != null) {
                Calendar cal = (Calendar) value;
                return dateFormatter.format(cal.getTime());
            }

            return "";
        }

    }
}
