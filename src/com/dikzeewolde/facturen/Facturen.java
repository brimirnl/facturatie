/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.facturen;

import com.dikzeewolde.db.AbstractDbRow;
import com.dikzeewolde.db.AbstractDbTable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jEROEN
 */
public class Facturen extends AbstractDbTable {

    public static final int NOT_PAID = 0;
    public static final int PAID = 1;
    public static final int ALL = 2;

    public Facturen(int type) {
        super();
        try {
            Statement stmt = conn.createStatement();
            if (type == Facturen.PAID || type == Facturen.NOT_PAID) {
                PreparedStatement prep = conn.prepareCall("SELECT * FROM invoices WHERE full_paid = ?");
                prep.setInt(1, type);
                rs = prep.executeQuery();
            } else {
                rs = stmt.executeQuery("SELECT * FROM invoices");
            }
            rs.beforeFirst();
        } catch (Exception ex) {
        }
    }

    @Override
    public boolean next() {
        try {
            return rs.next();
        } catch (SQLException ex) {
        }
        return false;
    }

    @Override
    public String[] getColumnHeadersForTable() {
        String[] ret = {"Factuur nr", "Klant naam", "Datum", "Totaal excl", "Totaal incl", "BTW verlegd"};
        return ret;
    }

    @Override
    public List<List<String>> getColumns() {
        try {
            List<List<String>> data = new ArrayList<>();
            rs.beforeFirst();
            while (rs.next()) {
                Factuur factuur = new Factuur(rs);
                List<String> tmp = new ArrayList<>();
                tmp.add(factuur.getId() + "");
                tmp.add(factuur.getClient().getFull_name());
                tmp.add(new SimpleDateFormat("dd-MM-yyyy").format(factuur.getDate_added()));
                tmp.add(factuur.getTotalexcl() + "");
                tmp.add(factuur.getTotalincl() + "");
                tmp.add(factuur.isBtw_moved() ? "Ja" : "Nee");
                data.add(tmp);
            }
            return data;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public AbstractDbRow get(int row) {
        try {
            rs.absolute(row);
            if (rs.next()) {
                return new Factuur(rs);
            } else {
                return new Factuur();
            }
        } catch (SQLException ex) {
        }
        return null;
    }

}
