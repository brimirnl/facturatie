/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dikzeewolde.facturen;

import com.dikzeewolde.facturatie.MyTableModel;
import com.dikzeewolde.klanten.Klant;
import com.dikzeewolde.klanten.Klanten;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author jEROEN
 */
public class selectClientFrame {
    
    private JFrame frame;
    
    public selectClientFrame() {
        init();
    }
    
    private void init() {
        frame = new JFrame("Selecteer een klant");
        Klanten klanten = new Klanten();
        MyTableModel model = new MyTableModel(klanten);
        
        JTable table = new JTable(model);
        JTextField search = new JTextField();
        search.setPreferredSize(new Dimension(200, 20));
        
        TableRowSorter<MyTableModel> sorter = new TableRowSorter<>(model);
        table.setRowSorter(sorter);
        
        search.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e){
                String reg = "(?i)"+search.getText();
                try{
                    sorter.setRowFilter(RowFilter.regexFilter(reg));
                } catch(Exception ex){}
            }
        });
        
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int row = table.rowAtPoint(e.getPoint());
                    row = sorter.convertRowIndexToModel(row);
                    Klant klant = (Klant) model.getObjectAtRow(row);
                    addFactuurFrame.setKlantAndUpdate(klant);
                    frame.dispose();
                }
            }
        });
        
        JScrollPane pane = new JScrollPane(table);
        pane.setPreferredSize(new Dimension(600,400));
        gbc.gridy = 0;
        gbc.gridx = 0;
        panel.add(search, gbc);
        gbc.gridy = 1;
        gbc.gridx = 0;
        panel.add(pane, gbc);
        frame.add(panel);
        frame.setVisible(true);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setResizable(false);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width / 2 - frame.getSize().width / 2,
                dim.height / 2 - frame.getSize().height / 2);
    }
    public void toFront(){
        if(frame.isVisible()){
            frame.toFront();
        } else{
            init();
        }
    }
}
