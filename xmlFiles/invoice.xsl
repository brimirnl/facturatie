<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="xml" standalone="no" omit-xml-declaration="no"/>
	<xsl:template match="invoice">
		<fo:root language="IT">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="A4-portrail" page-height="297mm" page-width="210mm" margin-top="10mm" margin-bottom="10mm" margin-left="10mm" margin-right="10mm">
					<fo:region-body margin-top="60mm" margin-bottom="25mm"/>
					<fo:region-before region-name="xsl-region-before" extent="60mm" display-align="before" precedence="true"/>
					<fo:region-after region-name="xsl-region-after" extent="25mm" display-align="before" precedence="true"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="A4-portrail">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table table-layout="fixed" width="100%" font-size="10pt">
						<fo:table-column column-width="proportional-column-width(105)"/>
						<fo:table-column column-width="proportional-column-width(105)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell text-align="center" display-align="center">
									<fo:block font-size="20pt" font-weight="bold">
                                                                                <xsl:if test="data/credit != 0">
                                                                                    <fo:inline>KREDIET </fo:inline>
                                                                                </xsl:if>
										<fo:inline>FACTUUR</fo:inline>
									</fo:block>
									<fo:block space-before="3mm"/>
								</fo:table-cell>
								<fo:table-cell text-align="right" display-align="center" padding-right="2mm">
									<fo:block>
										<fo:external-graphic src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPUAAABdCAYAAAB9/iDmAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAAJcEhZcwAADsQAAA7EAZUrDhsAACPoSURBVHhe7Z0LvE1VGsAXlZBoiiF5pGIYSqZCklFEoUISjRJCHpUx4xEZr+RZpodHpTHK1FTKq4yaSEmUmMqr5JWkBtWUVB5Zs/7r4eyz7z7nnnvd63Hu+v9++969115777XXXt9a3/r2Pt+XT1arJsXq1SKSSy4Rol8/IX7/eyGKF7eJHo/nWCafbNtWigULhNixQ4izzxaiRAkh9u0T4uSThVi7VogffhDi9NOFeP99ISpUsId5ErFA1eXWrVtFhw4dbIrHc2TJL555Roj27c3W4MFCvPmmEK+/LsT8+UJs2SJElSpCfPONEHXqCPHFFyZfDkCjL1KkiFi2bJlNSQ9eeuklMXz4cLuVfcaOHSsKFiwopk+fblPyBitWrBC//e1vlWJYXC9v0h6PIvOVHJysBrj777/fphxdvlAyWKtWLfHrX//apmQkv/576qn6n2pFQhQqZFTtX/1KiDPOMKN1xYpCfPWVELNmmXw5wNSpU0X//v3Fpk2bbEp60KlTJzFgwAC7lX369Okj5syZI1auXGlT8gYXXXSRanJrxa5du3Q9fvrpp3bP0eHqq68WM2fOFJs3b7YpR5ezzjpLvPvuu+Knn36yKRkxQn3woP536H+YxYvN/1GjzP8cYv/+/SJ/flOEdOF3v/uduP322+3W4fHzzz+LE044wW7lPWgfx8L9f/fddyJfvnx269ggWb2kJlGM4NWrC7Ftm02I5hulpv/973/XqgrqAb3KtddeK9avX29zJKd3796idOnS4rLLLhMbNmywqUeXRo0aiTJlyoizzz5bL5UqVdLbX3/9tc1haNmypS4798woG8Vbb70lihUrJp588kl9f+XLlxdnnnmm+MMf/mBzJGfSpEn62ueff75YunSpTc097rjjDn1PpUqVEn/84x9tquHbb7/VdVO2bFm9TUfGvVC+IzllmD17tq5Hytm0aVOxd+9euyceRvyKSuOkvOeee67YgQ0pAdw391yuXDmtNfwKrdVyzz336OnBxRdfLP7zn//YVAPt9je/+Y247rrrxPfff29ThThw4MCha3PeJUuW2D0xrrzySn0Pt956q5BS6rTmzZuL0047TSxfvlxvJ4N65/g///nPghNIOWQIp5HyH//Qmxn4/nspa9c2eZKg9H3ZtWtXqRqcTZFyxowZUl1Qqkq1KTH+8pe/yGeffVavqxuXl156qVyzZo3edqiHJJWqIdWoFbeQtm/fPpsr91DzfrtmWL16tWzbtq3cs2ePTcmIeoB2LSMLFy6UVatWVdVZW98z/PWvf9XnDKMarFQPyW5Jecstt8jnnnvObhl++eUXXZZw/bCQfvDgQZszdbhH1U7klClTbIqUkydPlqqjljt37rQpBqWZSNVR6WcJP/74o1RCo5pSgraUBUaNGiX/9re/2a2MPPXUU7Jx48Z2S8qXXnpJl3vdunU2xTBhwgSdTpuB3bt3SyWw8pNPPtHbju3bt+t7UZ2STZHy8ssvlzfeeGPcczjllFPsWkZUBy2Vemy3pOzevbtUo7xU0wmbIqUSfqmE127F2LZtm+zYsaNuIwUKFJBK+HU66wsWLNDrDsrp+O9//ytVRyOVZqO3UxPqH36Qsm7dTIU6EWPHjpX//Oc/7VYMGsK//vUvLZxXXXWVfO211+weAw+Bxq56P3nBBRfELWrElLfddpvNmXs888wzds0IUJMmTeIeWhgElQpOxKuvvhop9A0bNpTvv/++3TIg1IMHD9brNF6lBen1IGrOLdUIKatXrx5XP2yrUUELaFahIa5YscJuxaBj7tWrl90yVKtWTSrNzG4ZEC466MMlmVC/+OKLUmkKdiseBNixadMm3VaiqFmzZtwgQkf74Ycf2q0YtM1OnTrZLfOsHnvsMb1OB+EGJghei2srDcZuxTNgwAC5bNkyu2VgULzwwgulGrVtSmKcUM+aNUvWr19ft03HiaoCMgeVZuNGu5EcVBLUz88++0zPl1E3UVUfffRRmyOG6vHEkCFDhBJu8e9//1vnDXLSSSeJzp07a1UvPIdQNyHOwJCXy6hOxa4JoYRFqM5JqIZsUzKi6tSuRYNa1qBBA7sVA0tv+B4LFSokpk2bJtToLFRnoNXBMEwJ1EivLbRh1Git1bKsMnHiRP0cmU45mFOi2mKR55WdK4tq1FolDVKyZEmtEucmTPGokyjatGmjjVstWrTQUx3aUBRK8xFqUNHqNFNE2tN5551n98Zo3bp1nArMVINXl126dNHqP9tK8HUdBZ+h6jD0VIRjeRaOE088UVuxmRI8//zzNpU3yft03b3wwgs2JTGo5Uz5yP/GG2/YVIOZUztjlbpYJErgxJdfCnHzzTYhmnHjxmlD0YMPPqgL/PHHH4v33ntPqB5RGz3C0Eio+KFDh+oKCkMFXXHFFbrw119/fdxC2u/5KCYCpXLqc1NJOQGWRl4j0ACSCXSqBB+wgzKHofzM0+bNm6fvOQrmezS6cP2w3HTTTRk6ylRAcN955x39/JhTstBAN6qOnfWoziUIz5pONzdhnlq4cGG7FU+JEiX0QAD/+9//dIcZBR2hKyfPmE40ygBVtGhRu2ZQ6rh4++239TpvcOh0586dKx544IE42wPn7Natm/jggw8O1SPLRx99pMsXFGigDSCsUe0jjJrmaBsL1wifx0iz65Ex/rDwfnr7dqFm9EI88QRdn1A1KFSpTb4IlF6veyV6cUY0cBbDU089NXIEcwaEa665Ri8YGA4XDFA0Zs73Ax/O5AA33HCDePjhhw8ZhTgvr1yyS2ajuYMGxzUrVKgglCoqlOqv+lbVueYyGOJoMIC25RaeJ0KSyBgVJNV7zC50WgwGUTz++OPa4ARqiiaUqqzXwzDKKnVXr6vpin5tFdROHK+88kpcJ0VdoIkMGzZM3HnnnboT4Tilhov27psPRZUqVfRrSTTVYD2yQNSzRCZSqTs3qqvpiVDTVt0+HPnFpZcKpaOYrUGDhKhdW4j69YWoVw+TnFBDqNm3cKEQpUqZ9QgYEbD8fvLJJzaFj9De15X/hOoYsIyGQf12DYTK54MLesHDgWvOmDFDV9zpfAl3mDRr1kyXn5HaoeZduqxRoFoxbUgEbwV4GGEYHcPlpdd22oaaU2tVnHrKbRh1qMP77rvPphgeeeQR3WiDGhD3G4b7i7rHrMLHSeFR0oFAM9oFVX9GQaYj1JMrF1ZqNb8/JLwONEC0RCz4jpEjR+qpUfBtDSMxGktY46lbt67OT/uuWrWq+Pzzz3WnUKBAAZtDaK2OwUDNebWm49i2bZseKJ7hw68AtA2s48k+LHG4joH/U6ZM0delM9PIypWlLFhQyqJFpSxc2KyzFCggZcmSUjZvrjqO1MDCx6RdqTHqFAWlqjip1Dipen26HjlmzBibU2oDAmlB41i7du10Gsv8+fNtatYYPny4Pt5Zlg+Hl19+WVseVU8slbahF9XItDVTqVU2l5SqgUlVuXqfeviqGgtLJZD6/znnnCPVnErnw4JJ2ZTQS9UYdBpce+21UjVCna4apE5TUxidd/z48XobRowYodO4FvtzEyyxWHx5jrwBwPJ911132b1SqpFJqoaty4JBDgMgqIaty8hxasTXaamiNCCpplT6WOqR61LnrCtVOdIw1qdPH93esEgrgZZvvvmm3RMPFnCeD+ckP4a2KHhjgEXfnfOhhx7SBizaa7D9qhFY51MdnN7mrQ95oli0aJGWC1eXlZXMYUx0IAOqE9T7WLhfykp7aNOmjc1l4Llg4KOOg7BNW+XYfDYhLWAux3vkSy65JMM8w+PJK1gLWXqgelmxZcsWPZ/2ePIqaSXUwCuoVq1a2S2PJ++RVuq3x+NJw5Ha48nreKH2eNIML9QeT5rhhdrjSTO8UHs8aYYXao8nzfBC7fGkGV6oPZ40wwu1x5NmxL4oe/BBIXbuxDMBP4TWSYc4cMC4Ee7end8D2kSPx3MsYoQaF8D8fjozyFe3rt3weDzHIrGRetw44wGlSBG8AAjBTxevuAIfufiEEaJ0aZwzGQ8oRxi8TlDMqB/kZ4evvvpKO8nnfPilwtNH8MftQfD/hFMA3NxUrlw5Wz6/wuDdIuzPKhnkxddWx44dbUr2cb603Y/sjwQ4rsD9Do76cYyR0+DQAecarilnFzyUUFYcLeDq97gFoc4ADgrYNWyYTTi64FXTeW/MCXC92qJFC9m+fXt53nnnRXqQdHTp0kV7ksRrZufOnW3q4aEENOGP9KMoXbq0fP755+3W4YFDh7AXy9zm0Ucflarz1I4FcgM80uJM4XD56KOPtItePLEez0R314zMkIIDtCMB7l/WrVtntw6fOnXq6JhXBB5g9Ai65wmDfyvcxeD1NKdGN1w+4cstVQizgtucnIBYVc4p35GiR48e2n0PTvJyA0LjrE4UuTULoLGhER1JLSY3iC592FCWAqjIeE10XhZptIsWLdLRCFC9EoFnRYKg4XUUgg8HVRGvltu3b9cOEFCZOT+RLmicYa+LNBoaLEH3OGdmHQFlxoOjc5CYDM6dLB/7cSRH+bhvfFaFwWkfZcOtLo4aUfXc/eD3LOxRFDe0ixcv1udFZc8M8nOuVatW2ZQYPA+uR4PFBxf1zbWpJ3xwRUEZ2c8UZCdG1CTwPMjrAh7iTTYI5XeqN2XknEyDgvC8uQdXdzxvzsm5o8AhBnXNfVCnmUFb5NrhyBpBaGdBcOlMNBSOi3JKeCwS/Xtq5tM33SQEgd5GjLCJyUFwJ0yYIPr27av9MeMuFfe1VBJzOOY9wTkx8Ynwx8xcFuGkGHgWHT169KG5EZXYrl073bjIj7tcHp7zsjlo0KA4B3c4kTvnnHP08YRVwYUsjQkXrlGw7+abbxb9+vXTI3Yy8JRKo3BeNsPgGhnfzzRIGif3itO8oCdLGhMO8+hscEeLYzrqjfyMEvg/L0iII0u9evX0/A4XwXiLxL90FAgITgE5lnOxUA9/+tOftDNIwNPlrFmztJAwX8QRJPmoe5zw6XAtFtLwJY7gcx90BKThQK9Xr142lwHBGz9+/KGOu0iRIrouyBdsWozWlA8vrFyXZ8izxKUu9QZs44oKb7TcN8ETceNLZzRmzJgM2grPhAWXxTiHjGrKgGZE3XIfXJvOEyeP1A8eP4PQXi+//PJDgQlffvllHToKJ4U4OaR8xzyqIjJCaBd2DRhgE1KHyAw4iVOjkk2ROsRI2Ckb81NVSXbLsHHjRp6K3YpBlIZ7773XbiVG9djys88+s1uG1q1bx4WPCaIesA6pohqNTUnM008/Le+44w67lZHXX3897p6VEEil5usyhRk2bFiWbAT33XefjnISxYYNG3RkCZwkBhk5cqQOGcM9BiEsjOoE7FY0SuPSzxHngg7C7eBIMuh8Dwixo0axOEePPIPwc8QxX/HixeMitXBOnDgGw98QBQQnh8GIGKqD1tcOR3AJogYQuxaP6jxkrVq1MkT6YB6Os7/gMwOcH+JQ0IHjyOw6wTxa5PjkgcgD9OrFAi5V77777gzzOFy3MgKhXjOC4Sie0SXKpzRpqfiaZiTHxzgjDD0tLmMZhZgW5Da4lkVVY1Th2kQoIS0quBn1k8r9ONAoEqn+qIa4MWbE4bos3C9B97DWh1VX7AdcPxmMSvjERutw50TzGThwoFbjKQ8QvAGXxYxsQUs+I2cwQBwwQhKlBZ/sDpzsM1I7lR3Qzgj+gB3DgV9tfMInCycbFQwBaAtoQTwL2pmrHxxUolWGo2FQz5QVrQFNinaJe+bjiWihTqDGZBcqKGx8ICIHKhm+pVFrUO1Q25hbRqE6ILuWGFRIIhai4tIA1eiu/S6jEuY2hHBBnUPNI64y13/qqad0g4wilftxJMvrnLkjHFyThfqkHuhYovxvZ3ZthIe6o9G7c3JvRF4B10EzJ65Ro4ZeD0PnGibKHzrTquDrRNTyqOeFoKX6CjAIhi+EmYHFPRfqp2fPnjrUU/jVFdMN7AEETsA4ejwSLdTuXWISp/RZJaohMadmtCZaPwYP5p8IdziMLcdmZsxiXkyvyvyOiAqEyMHCzdwu2aiYFeFKBA0A+wGB9N39MAIwB4yKEpLK/aQKc2OcLTKycs9uoQ6wbTA3DZLqtXkW3Ef4nMzdXUeVFW2D6yKwYcJlYdtpAjkB0U0YLIitFbwXnhP/0TKCcG00RgyUtJ+oGHDHOjGhRg1RI4uYM0eI1183aQiXepBi2jQhZs0ygfIyIWgMc9DzYhgKEu51iRmMEQJPoKhIYcIqI8KCqu8gvAkNkRi9DlRNjFuJYi5RVsqVygcR5Anfg4PGgUYQPA+WbrQOomxEEX6NRv5EIXUof6J7IBghHRpvEcLg+zxshUdNDXdkWLaD8bYxkvEKKgzPgI4CzQsYye66667Ijgs1+6GHHrJbJthfVD2H0xPVM0a2RPUPiUZxtDcidhB1JAwGxvD0iHp2xkoiaNBGMMgeTxjrN69AbPyrpBAor2FDuxEPjRRLIY0ByzdB2/j6igbQqVMnPY9h1OzO9+MKemQaBQ+K3hErJ5XIHJi5WhAaHPNlRnaugyrGu1tCqYyw1nlGScKgdO3aVeehoaF+8roGtY8Hw4jKPtYRNhr4008/redbxEZiHki5XSwwrMWUm06J66EJEKMJCymjDqo20HDo2fmqifOjnvJuHUHjvJSLWE0OrLEEFMea7crKXJz3rS7AGiME5+HajMLUD9Z9rk29Br8uW7hwoejQoYOezlCXTHXoGLmfyZMnx43W3C/TAs5FXqzRXBuVlOs7qEfUcOqGemJUxmrP/TC9cQKGZsTrHt4eIFjkI8QMHYATpBdffFHXFaMmETVdgD1GQtI5J+WkQ+ZtB2VneobKD3TM1DHXxAqOPYZnwb3wbNGSuHfevlBnPBvOw4gL5OXZ0zaZFlA/2FuwE6CSu9Ga9sLAQCftXnsxx6fcPFvqjPZxrHOCEqwhgpCwlSrxFp/IY0IQqC64NGuG9cQIdOCVSxAqksaB+kxDRTB4pUMjRLB4PUMjIq4RoMLRm9PASacR4ISfkScMvSfGIB6O+4CBRk3IUtdDM7djfuTej7LO6ypGEh40D5OYR5STd6Gck2Nr166ty+kaLmFNiWcE7sHTCGh4dCLkoUOiY6EDAh42Qkde9iHABE5D62Ck5FVd8PNSDERE7ERoOB9lo8MikqcDAxjXoFyUCU2Gbfpgyh58BUejo37pQBFkXi9x78yJw+F+KRvhWnnHy/kQMCIz1qlTx+YwIOCUmzrg/ik/z4eOKDg35nliiELL4NkwsiFQwefIaMi5uG+egSsT6ZSFdO6ReqATpW5oE3TkwHSMzo3OifLyOpNnyvt4rsd9UHdOg6DOaH8IO/D6irKTj06MOuc1Gh0FBjMH2hIdEuV36Qg652GhfK7DP5aJfk/t8XiOW3L8lZbH4zm6eKH2eNIML9QeT5rhhdrjSTO8UHs8aYYXao8nzfBC7fGkGV6oPZ40wwu1x5NmeKH2eNIML9QeT5rhhdrjSTPyi5kz+R0kP5vi50NmYdv9trdfP354HNuPV4rp080+j8dzzJFfuB/R79rFD5fNAtu2mf94icRjhdu/Zw++Y80+T7bhZ4q4B+KngB5PTmJ+eomg1qyJI2Xj+aRyZSGsa1mBT2jnRnXGDCHq1zfB8hKEqeE30viV5nfF/KC8f//+do8nCL+z5jfa+Dnnt8QeT05h5tS4k0HFZkTmh+UI9LffCtGqlRHoc881IzcubviBewKBhsGDB2s/23iiwMe0JxocM+CJI8rFj8dzOMQMZc5XAnPmAweIQYIfGiFuvRWP5rGROxMYdfAagctaGq4nGlwo45yQ0drjyUnird8I9Pz5QlStigNvIbp1M04HUcezCC6NEjlVwZ9W06ZNdQQGB/7JcHWDz+mwN1HA7xkufJxbH/xb4QcsDOos5yAvnprIQ94ocJeDj2nc6UbB1AFPlPjrwtUN4GET9zguZAw+tnCYiO8yXCrhbRO4Nr608L9dq1Yt7csL8FuG6yEWXCnhAikZ5MdtEq6U8MEFQd/ZQXD3Q1n8lCePw5xaU6MGIphxefhhmyFrEAWC6BdRbN++XapGHRetQQmfXLdunWzQoIF89tlnbarUERqJkjBw4EC5Zs0a+cUXX8gSJUrIwoULy7Vr19pcBtVZ6OgKRFRQwi0nTZqkbkHoyJVBiDJB+ooVK+SqVavk448/riNLhqNPEDmC6B0NGzaUc+fOlaVKldL3tGfPHlmhQgW5dOlSuWXLFh0tkTItX75cbt26VR+7fv16XT6OmT59ui437N69Wx+3ceNGHXWCYxOhOiZdT0uWLNH5iOqhhFuWLVvW5jAQ/YL7UR2Nvp9Ro0bp8qiO0ubw5CWU1FqcUNesSZwbKQmTwzbL1Kk2U+okE2qgIT4fEZ61W7duccJOaJ4RI0bYrRiDBg3KEGIn2Ec55s2bp4XWceDAAS2kUfTs2VOuXLnSbsVo3ry5bNKkiZwxY4ZNMXz33Xd2zXQoSiOwWwaETI2cdisjhARKJNSvvPKKDpETBWGMHHR0hOSNokePHvLLL7+0W568QkwKLrhAyhNPlPLdd22ColUrI9TEKVIjTFbITKgRgiihJobSc8Tysvz444+qaBfokee6666T9evXt3sygiAg2MSQYsRnpA5DHCtGzxtuuEE2btz40MK5zz//fB1LOUzLli1VtQTqJQIEOqwRVKpUKalQUYZEQk08qURQJ462bdvKKlWqyOuvv/7QvTRq1Eg9ula6zhjlPXmLeEMZ7naD4XFw8E9EQizhPXvaxJyBV15R4EhdlctuGWfvOMXHqESIHtz3cizz0WA+wLczroaxwOObGif35CVel4MImsxJmQvj69otvXv31g75iXYRBlfCicrrYB6PO1ncJAOhcPA77dwNZxXC0SQi6NQel8DYKPBl7u4FX9aElaHOsAl48hhatOHii6UsVEjKjz+2CRa2jcgz8bWJmcNIzWiRCFTPcKRGIILjzJkz9fr+/ft1BMaffvpJbwdp1qxZXOTIzZs36/ltGCIrBm9z9uzZeo6eFbiPzEZqeOSRR2SvXr30Oip+MJpjFMlG6qZNmyaMxkkkzc8//1yvMw3hPB6PI78Op0OQsA8+4DMn47gfx/POAs0+0oAPT3hnbS28ycBhe9iRfBCc8xMJwYFFmzQiIOLg3aVhVQ6OtA4s5MHIExyHE/gwhOPB2byD6IlKRdUO7MOQRsC5MJndi4PRkegXgON3pULr9UQQGC4YHTQIUS2wnrvgBQ4ihXAPzlG9mk4InNjzdiAIzvB5w0B+Tx5D3n+/GYXPOEPK4sWlLFbMbM+ZY+VegUGKtKJFpcyXj4DJdkc8EydOlErQtHGIeV5RlZ84x1hxTznlFJvLgAWZfJUrV9YW6yJFishp06bJ8ePH65F18eLFeqRm7l2uXDk9r2XOyzHMN8OWXSzMjG5cF6tyjRo1ZMWKFeNiDQfBUMeck/PVrFlT3XYxqdRvu9dY4zGOcTz3Ua1atUPlZd6/b98+mzMeN68PWvAdSvXXc9+qVatqjYS64trUD3Gehw8fbnMasJ6rDu7QtcuUKaPtBVGQXr58eX0uzqk6Iak6BrvXk5cwn4kSqiYwTxMEPAu+m1Yjpv6ElJA7zLsDQeiC8M6V8CuEZSEcCv8ZaZiPEn6HkScMoXSYB/OOGginQkgYRmtGO7YZlTiekDoUNyp8KvGouB7hbzgnoWcYBV3cpig4F/N1NAIlNDbVQBphcfhPmBveuxOyh4UQMIS6SQRahNM2gjB6ulA7hLLBfsB5CRPDPsLPBAP8OQhbRB0RuibZF2jUAffO+Xn/7smb+LA7Hk+aEf9FmcfjOe7xQu3xpBleqD2eNMMLtceTZnih9njSDC/UHk+a4YXa40kzvFB7PGmGF2qPJ83wQu3xpBn+M9EgVMXGjebXau535T//LMRFF5n1rLJqlQmMwLfzJUsm/Gbe48lJjFDjVO+110yjDjsDcI2Sn/o1bCjESSfZHWnIG2/g7cDcoxPqgweF2LnTrGcFnCU0aWLWCYjQvbsQY8aYbY8nFzFCvXSpEFdfjRsNmxwBDucXLBAim548jgu4PzquMNlRZvilW9myQneYwO+3J0406x5PLmKGI0YmGu7JJxuH/U2bmgXXPu4nhOwL/jwzHWnQwNQDi/N9nl1wt4wbqBUrzDbBEjyeI4ARalRsGjFeTgitg/N+ltGjY2roY4/hgd6sR4HHj7lzceBt1M1UoMETMIBrrl1rE3MJ/Jc/91zqqjQxxhhts8rs2eZ+nDCnUhdTp5q6o3xESfF4DgfUb/n222Z8Cvru2rvX+C0jfcoUmxgB7ntLlnTjm1kKFJDyssukTORJ84UXjBeV4DEs5crF+0jbuVPKggWNRxaWk0+WcsECu1OxY4eUp50m5emnG88tpUtLuWuX3ak4eBAXJ1IWKRK7Bp5bLrxQyk8/tZkSgL8z6oNjUqF5cylPOil2HZYzz5Ty3nvN+p132owB+vQx5Q4ewz3iwnjPHpvJ48kaqhVZbrlFypEj7YYCd7c0skaNbEIEPXvGGiNudmiMV19tXAq79FWrbGZL0J84wnXVVeY4BNqlOyd/33wjJW6QXDpCQgd04IDZx3pQKM46S8rFi82xv/wiZePGJh1hZ51ruY6KZf58kzeKVIWaToROjHzcA/fCdbg3dx2WsFC3bh3bd8UV5jjqGuePLt0GAPB4soJqOREwMtOoaJiMdlE432bVq+MgzCZa1qwxIxf7S5SwiYonnzRpLAQI2L/f7lAQ2WLyZLMPQd2wwaTjNbNCBSPcDoSgSROzvn69lA0amOPcMdC5s0lDmENO/7WAuXIkIlWhvvlmk4fOAs3CQcczdmzsOkGhdp1hvXpShgIEyOXLY/dD3Xo8WUS1nBBEx6BBoe4GnMbHwSiIoKEqIsCJqFVLyvz5pVyxwmy70TgUySKOfv1MnuAoyiiGagvvvGP2szhVnHLUqWPW4eefzf5SpeIFPQgaBXmGDLEJIVIR6qVLjTqfLA/BB9jvhBqHhUwpmLLYED2RoHXQuYVdNns8mWCtYJa9e41BDPr2jVm7CTbPe2ziWMN77xmDExZxgrbxOuyaa+KXli2FKF7cvOedNMkct3Wr+QgDYxrvcMPHsLhXQIMHm/+Aa2LelfMeGYMXYF3GILV7tyl3x44mHTA6AdfApXEU48aZ/ytXmv/ZgeD7iGyy988jRtgVy8KF5oMW3jgMHx5dd7yBwBEj9U5ABY8nK1jhNhBHi6S777YJFrZJdyFc+B9l6Eq03HabOS5qX6LlkkAMKpzik9a9u1G9y5eXcsIEk8Zozf9XX7WZFYTtIY15ayLcVKBpU5sQIpWRevZss3/oUJsQwVtvmTxupJ43T8oTTjBpqSyDB5vjPJ4UUa3G0revaUQtW9qEAO3amX2ovo6KFaUsU8ZYyVEjN26MXzZtMnNZ/n/9tTmGc3OeRYuIHmf2RR2zeXNGIxHHOYMUQewQDtbpXFBlQxEw9T5U9g8/tAkhWrQweSL8c2uw3J97rsmTiNdeM/uxwCfCze1t5A4NxjCmJkQeoe6YIgTrgfunHlgPBOHzeFLBqN9DhxoVkq/Gpkwx71hRsVnWrSPYtM4W9wnp7bcLQXzoRo2MKk5UjxIlzMJXZ/gI37XLfIDhjncqNer0N98IgQ9r1HF3DOo8KifHhT9HrVMndh78fuNDnA9j+AoONbtKFbPPceONRmWvXl2IJUvM1AHVnsgj7dsLMXOmuXabNvYABdEsWFDJWZg6wObNpi6WLcMJt0kD4oy1a2fOW7eu+W6c8rNN7G3ewT/xhMlLbKzVq806X5dxHXcsvsmDdce3AdQB+7gHjycraNHu39+MJpktvEIK4oxaLHXrGmstC6qzi/TBwrtiB0YzZ1wisiNW46hjgiMbEDeL9GB0EN6Rk/bAAzYhBK+I3PkoH6Oj22Y9GLYWo6Dbl2xhlGUEdbz3njHSuf28MahdO7bdo0dsnWX0aHNcx46xNKzgru6YAgXfdyeIhuLxJMJ8+z1vnhDz5xMAyop6CEboHTuEuOceIcKRKfjhAqNa1642wXLppUIQ24pPLxlVTzvN7lAwanEu4nh9+aVNVHBuRi9iUHXoYEZyB8fcdpsQo0aZL9+AEfTBB41Rj++so2B0nTNHiJEjzTaRONAyOneO1zwAox/nRGOIAk2BkbR37/hjSef7eUZgNBu48koh+vcXgqiTAweaH8QUKCBEly5CnHWWycPo/9ZbQvTpY7YdGM/QMIhhduGFxijo8aSEEP8HX2btfATeOLMAAAAASUVORK5CYII=" />
									</fo:block>
									<fo:block space-before="10mm" />
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<fo:table table-layout="fixed" width="100%" font-size="9pt">
						<fo:table-column column-width="proportional-column-width(105)"/>
						<fo:table-column column-width="proportional-column-width(105)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell text-align="left" display-align="center">
									<fo:block font-weight="bold">Klantgegevens:</fo:block>
								</fo:table-cell>
								<fo:table-cell number-rows-spanned="5" text-align="left" display-align="center">
									<fo:table table-layout="fixed" width="100%">
										<fo:table-column column-width="proportional-column-width(30)"/>
										<fo:table-column column-width="proportional-column-width(70)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell text-align="left" display-align="before">
													<fo:block>Factuur nr:</fo:block>
												</fo:table-cell>
												<fo:table-cell text-align="left" display-align="before">
													<fo:block>
														<xsl:value-of select="data/number" />
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell text-align="left" display-align="before">
													<fo:block>Datum:</fo:block>
												</fo:table-cell>
												<fo:table-cell text-align="left" display-align="before">
													<fo:block>
														<xsl:value-of select="data/date" />
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell text-align="left" display-align="center">
									<fo:block>
										<xsl:value-of select="data/name" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell text-align="left" display-align="center">
									<fo:block>
										<xsl:value-of select="data/street" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell text-align="left" display-align="center">
									<fo:block>
										<xsl:value-of select="data/zip" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell text-align="left" display-align="center">
									<fo:block>
										<xsl:value-of select="data/country" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:table table-layout="fixed" width="100%" font-size="10pt">
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell text-align="center" display-align="center">
									<fo:block>
										<fo:inline color="red" font-weight="bold">DIK </fo:inline>
										<fo:inline>VOOR MEKAAR!</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell text-align="center" display-align="center">
									<fo:block>
										DIKzeewolde | 06 - 13 12 47 61 | contact@dikzeewolde.nl | dikzeewolde.nl
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell text-align="center" display-align="center">
									<fo:block>
										BTW: NL474193548B01 | KvK: 61566055| Rekening nr: NL.26.INGB.00.06.61.87.35
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" border-collapse="collapse" reference-orientation="0" font-size="9pt">
					<fo:block linefeed-treatment="preserve">
						<xsl:value-of select="data/note" />
					</fo:block>
					<fo:block space-after="10mm" />
					<fo:table table-layout="fixed" width="100%" font-size="10pt" text-align="left" display-align="center" space-after="5mm">
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-column column-width="proportional-column-width(135)"/>
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-header>
							<fo:table-row height="8mm" border-after-style="solid" border-width="0.5mm" border-color="black">
								<fo:table-cell>
									<fo:block font-weight="700">Aantal</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block font-weight="700">Productomschrijving</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block font-weight="700">Prijs p/s</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block font-weight="700">Totaalprijs</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-header>
						<fo:table-body font-size="95%">
							<xsl:for-each select="products/product">
								<fo:table-row border-after-style="solid" border-color="black" border-width="0.35mm" height="5mm">
									<xsl:if test="borderWidth">
										<xsl:attribute name="border-width">
											<xsl:value-of select="borderWidth" />
										</xsl:attribute>
									</xsl:if>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="quantity"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="name"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="each"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="total"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:for-each>
						</fo:table-body>
					</fo:table>
					<fo:table table-layout="fixed" width="100%" font-size="9pt">
						<fo:table-column column-width="proportional-column-width(150)" />
						<fo:table-column column-width="proportional-column-width(30)" />
						<fo:table-column column-width="proportional-column-width(25)" />
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell><fo:block/></fo:table-cell>
								<fo:table-cell>
									<fo:block space-before="140mm"/>
									<fo:block>
										<xsl:choose>
											<xsl:when test="data/btwMoved = 1">
												<fo:block>Totaal excl btw.</fo:block>
											</xsl:when>
											<xsl:otherwise>
												<fo:block>Totaal incl btw.</fo:block>
											</xsl:otherwise>
										</xsl:choose>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block>
										<xsl:choose>
											<xsl:when test="data/btwMoved = 1">
												<fo:block>
													<xsl:value-of select="data/totalExcl" />
												</fo:block>
											</xsl:when>
											<xsl:otherwise>
												<fo:block>
													<xsl:value-of select="data/totalIncl" />
												</fo:block>
											</xsl:otherwise>
										</xsl:choose>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:if test="data/btwMoved = 0 and data/credit = 0">
								<xsl:for-each select="btwamounts/btw" >
									<fo:table-row>
										<fo:table-cell><fo:block/></fo:table-cell>
										<fo:table-cell>
											<fo:block space-before="140mm"/>
											<fo:block>
												<xsl:value-of select="name"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<xsl:value-of select="price"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:for-each>
								<fo:table-row>
									<fo:table-cell><fo:block/></fo:table-cell>
									<fo:table-cell>
										<fo:block space-before="140mm"/>
										<fo:block>Totaal incl BTW: </fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="data/totalIncl" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:if>
						</fo:table-body>
					</fo:table>
					<fo:block id="end-of-document" />
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
